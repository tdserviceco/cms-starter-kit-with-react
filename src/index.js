import React, { Component } from 'react'
import {Example} from './components/example-component.jsx'
import css from './app.scss'

class App extends Component {
    render() {
        return (
        <div>
        <h2>Im a react test Component</h2>
        <Example />
        </div>
        )
    }
}

render(<App />, window.document.getElementById('reactJS'))