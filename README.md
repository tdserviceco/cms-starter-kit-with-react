### Setting up ###

You can change folder name to Webpack2 if you want
Then inside the folder start typing: npm start it will start the installation + activate webpack2
If you need to start the Development mode then type npm run dev (heads up, you get alot of update files in your dist folder since --watch is on)
When your 100% done or want to see how production is on live server just type npm run prod and is will clean up dist folder and replace everything with minified (s)css/js(x) and the images that you used in the scss/jsx files

### Commands ###

Full installation: npm start

Developer Mode: npm run dev

Compress to Production: npm run prod